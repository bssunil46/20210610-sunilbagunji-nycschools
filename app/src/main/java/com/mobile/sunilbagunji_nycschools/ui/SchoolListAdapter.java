package com.mobile.sunilbagunji_nycschools.ui;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mobile.sunilbagunji_nycschools.R;
import com.mobile.sunilbagunji_nycschools.models.SchoolModel;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Class implementing the custom adapter to display a list of schools.
 *
 */
public class SchoolListAdapter extends RecyclerView.Adapter<SchoolListAdapter.SchoolViewHolder> {
    final private SchoolItemClickListener mSchoolItemClickListener;

    private List<SchoolModel> mOriginalSchoolList = new ArrayList<>();
    private List<SchoolModel> mSchoolList = new ArrayList<>();

    SchoolListAdapter(SchoolItemClickListener schoolItemClickListener) {
        this.mSchoolItemClickListener = schoolItemClickListener;
    }

    @NonNull
    @Override
    public SchoolViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_school, parent, false);

        return new SchoolViewHolder(view);
    }

    @Override
    public int getItemCount() {
        return mSchoolList.size();
    }

    @Override
    public void onBindViewHolder(@NonNull SchoolViewHolder holder, int position) {
        SchoolModel school = mSchoolList.get(position);

        holder.txtSchoolName.setText(school.getName());
        holder.txtSchoolAddress.setText(school.getAddress());
        holder.txtSchoolPhoneNumber.setText(school.getPhoneNumber());

        holder.itemView.setOnClickListener((v) ->
                mSchoolItemClickListener.onSchoolItemClicked(mSchoolList.get(position))
        );
    }

    public void setResults(List<SchoolModel> schoolModelList) {
        this.mOriginalSchoolList = schoolModelList;
        this.mSchoolList = schoolModelList;
        notifyDataSetChanged();
    }

    //  TODO:  Given more time I would prefer to move this out of adapter to adhere to Single Responsibility Principle
    public void filter(String search) {
        mSchoolList =
                mOriginalSchoolList
                        .stream()
                        .filter(p -> p.getName().substring(0, search.length())
                                .toLowerCase().equals(search.toLowerCase()))
                        .collect(Collectors.toList());
        notifyDataSetChanged();
    }

    public static class SchoolViewHolder extends RecyclerView.ViewHolder {
        ImageView imgSchool;
        TextView txtSchoolName;
        TextView txtSchoolAddress;
        TextView txtSchoolPhoneNumber;

        public SchoolViewHolder(@NonNull View itemView) {
            super(itemView);

            imgSchool = itemView.findViewById(R.id.imgSchool);
            txtSchoolName = itemView.findViewById(R.id.txtSchoolName);
            txtSchoolAddress = itemView.findViewById(R.id.txtSchoolAddress);
            txtSchoolPhoneNumber = itemView.findViewById(R.id.txtSchoolPhoneNumber);
        }
    }
}
