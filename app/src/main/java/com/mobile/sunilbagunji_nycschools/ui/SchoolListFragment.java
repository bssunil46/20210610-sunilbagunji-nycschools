package com.mobile.sunilbagunji_nycschools.ui;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.mobile.sunilbagunji_nycschools.AppDialog;
import com.mobile.sunilbagunji_nycschools.BaseFragment;
import com.mobile.sunilbagunji_nycschools.R;
import com.mobile.sunilbagunji_nycschools.models.SchoolModel;
import com.mobile.sunilbagunji_nycschools.viewmodel.SchoolViewModel;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import static com.mobile.sunilbagunji_nycschools.ui.SchoolDetailsFragment.SCHOOL_MODEL_KEY;

/**
 * Class implementing the UI to display list of schools.
 */
public class SchoolListFragment extends BaseFragment implements SchoolItemClickListener{
    private SchoolViewModel mSchoolViewModel;
    private SchoolListAdapter mSchoolListAdapter;
    private EditText mEdtSearch;
    private RecyclerView mRecyclerView;
    private ProgressBar mLstProgressBar;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mSchoolListAdapter = new SchoolListAdapter(this);
        mSchoolViewModel = new ViewModelProvider(requireActivity()).get(SchoolViewModel.class);
        //  Passing null as view should not have reference to repository.
        //  Can be done in a better way using dependency injection.
        mSchoolViewModel.init(null);

        mSchoolViewModel.getSchools();

        mSchoolViewModel.getSchoolListResponseLiveData().observe(this, (schoolModels) -> {
            mLstProgressBar.setVisibility(View.GONE);

            if(schoolModels != null && !schoolModels.isEmpty()) {
                mSchoolListAdapter.setResults(schoolModels);
            } else {
                AppDialog appDialog = new AppDialog();
                Bundle bundle = new Bundle();
                bundle.putString(AppDialog.KEY, getString(R.string.error_message));
                appDialog.setArguments(bundle);
                appDialog.show(
                        getChildFragmentManager(), AppDialog.TAG);
            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_school_list, container, false);

        mEdtSearch = view.findViewById(R.id.edtSearch);
        mEdtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mSchoolListAdapter.filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        mRecyclerView = view.findViewById(R.id.lstSchools);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        mRecyclerView.setAdapter(mSchoolListAdapter);

        mLstProgressBar = view.findViewById(R.id.lstProgressBar);
        mLstProgressBar.setVisibility(View.VISIBLE);

        return view;
    }

    @Override
    public void onSchoolItemClicked(SchoolModel schoolModel) {
        mSchoolViewModel.setSelectedSchoolModel(schoolModel);

        SchoolDetailsFragment schoolDetailsFragment = new SchoolDetailsFragment();
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .setReorderingAllowed(true)
                .addToBackStack("nyc_school")
                .add(R.id.fragmentContainer, schoolDetailsFragment, null)
                .commit();
    }
}
