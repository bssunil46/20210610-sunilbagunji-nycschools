package com.mobile.sunilbagunji_nycschools.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mobile.sunilbagunji_nycschools.BaseFragment;
import com.mobile.sunilbagunji_nycschools.R;
import com.mobile.sunilbagunji_nycschools.models.SchoolModel;
import com.mobile.sunilbagunji_nycschools.viewmodel.SchoolViewModel;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;

public class SchoolDetailsFragment extends BaseFragment {
    public static final String SCHOOL_MODEL_KEY = "schoolObject";

    private SchoolModel mSelectedSchool;
    private TextView mTxtSchoolSatScore;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SchoolViewModel schoolViewModel = new ViewModelProvider(requireActivity()).get(SchoolViewModel.class);
        mSelectedSchool = schoolViewModel.getSelectedSchoolModel();

        schoolViewModel.getSchoolDetails(mSelectedSchool.getDbn());
        schoolViewModel.getSchoolSatScoreModelLiveData().observe(this, (schoolSatScoreModel) -> {
                    StringBuilder satScoreStringBuilder = new StringBuilder();

                    if (schoolSatScoreModel != null) {
                        if (schoolSatScoreModel.getSatCriticalReading() != null) {
                            satScoreStringBuilder.append(getString(R.string.sat_critical_reading_avg_score));
                            satScoreStringBuilder.append(" ");
                            satScoreStringBuilder.append(getString(R.string.separator));
                            satScoreStringBuilder.append(" ");
                            satScoreStringBuilder.append(schoolSatScoreModel.getSatCriticalReading());
                            satScoreStringBuilder.append(getString(R.string.newline));
                        }

                        if (schoolSatScoreModel.getSatMath() != null) {
                            satScoreStringBuilder.append(getString(R.string.sat_math_avg_score));
                            satScoreStringBuilder.append(" ");
                            satScoreStringBuilder.append(getString(R.string.separator));
                            satScoreStringBuilder.append(" ");
                            satScoreStringBuilder.append(schoolSatScoreModel.getSatMath());
                            satScoreStringBuilder.append(getString(R.string.newline));
                        }

                        if (schoolSatScoreModel.getSatWriting() != null) {
                            satScoreStringBuilder.append(getString(R.string.sat_writing_avg_score));
                            satScoreStringBuilder.append(" ");
                            satScoreStringBuilder.append(getString(R.string.separator));
                            satScoreStringBuilder.append(" ");
                            satScoreStringBuilder.append(schoolSatScoreModel.getSatWriting());
                        }
                    }

                    if (satScoreStringBuilder.toString().isEmpty()) {
                        satScoreStringBuilder.append(getString(R.string.no_sat_score));
                    }
                    mTxtSchoolSatScore.setText(satScoreStringBuilder.toString());
                }
        );
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_school_details, container, false);

        TextView txtSchoolName = view.findViewById(R.id.txtSchoolName);
        TextView txtSchoolAddress = view.findViewById(R.id.txtSchoolAddress);
        TextView txtSchoolPhoneNumber = view.findViewById(R.id.txtSchoolPhoneNumber);
        TextView txtSchoolPhoneDescription = view.findViewById(R.id.txtSchoolDescription);
        TextView txtSchoolEmail = view.findViewById(R.id.txtSchoolEmail);
        mTxtSchoolSatScore = view.findViewById(R.id.txtSchoolSatScore);

        txtSchoolName.setText(mSelectedSchool.getName());
        txtSchoolAddress.setText(mSelectedSchool.getAddress());
        txtSchoolPhoneNumber.setText(mSelectedSchool.getPhoneNumber());
        txtSchoolEmail.setText(mSelectedSchool.getEmail());
        txtSchoolPhoneDescription.setText(mSelectedSchool.getDescription());

        return view;
    }
}
