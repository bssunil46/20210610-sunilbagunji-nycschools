package com.mobile.sunilbagunji_nycschools.ui;

import com.mobile.sunilbagunji_nycschools.models.SchoolModel;

/**
 * Interface to handle click event in the school list.
 */
public interface SchoolItemClickListener {
    void onSchoolItemClicked(SchoolModel schoolResponse);
}
