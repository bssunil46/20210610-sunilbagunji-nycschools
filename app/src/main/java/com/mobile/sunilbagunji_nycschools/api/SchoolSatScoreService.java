package com.mobile.sunilbagunji_nycschools.api;

import com.mobile.sunilbagunji_nycschools.AppConfig;
import com.mobile.sunilbagunji_nycschools.models.SchoolSatScoreResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface SchoolSatScoreService {
    @GET(AppConfig.SAT_SCORE_ENDPOINT)
    Call<List<SchoolSatScoreResponse>> getSchoolSatScore(@Query("dbn") String dbn);
}
