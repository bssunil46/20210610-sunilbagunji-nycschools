package com.mobile.sunilbagunji_nycschools.api;

import com.mobile.sunilbagunji_nycschools.AppConfig;
import com.mobile.sunilbagunji_nycschools.models.SchoolResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface SchoolListService {
    @GET(AppConfig.SCHOOL_LIST_ENDPOINT)
    Call<List<SchoolResponse>> getSchools();
}
