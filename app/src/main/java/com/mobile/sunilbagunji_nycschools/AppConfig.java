package com.mobile.sunilbagunji_nycschools;

/**
 * This class has all the app related configuration data.
 * We can have different app configuration for QA/PROD env.
 */
public class AppConfig {
    public static final String BASE_URL = "https://data.cityofnewyork.us/";
    public static final String SCHOOL_LIST_ENDPOINT = "/resource/s3k6-pzi2.json";
    public static final String SAT_SCORE_ENDPOINT = "/resource/f9bf-2cp4.json";
}
