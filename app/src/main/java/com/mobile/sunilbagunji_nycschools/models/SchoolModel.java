package com.mobile.sunilbagunji_nycschools.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * This class creates a model with only the data required for the UI.
 * It is created using the school response object which is the API response and has lots of data.
 */
public class SchoolModel {
    private String dbn;
    private String name;
    private String address;
    private String phoneNumber;
    private String email;
    private String description;

    public String getDbn() {
        return dbn;
    }

    public void setDbn(String dbn) {
        this.dbn = dbn;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
