package com.mobile.sunilbagunji_nycschools.models;

/**
 * This class creates a model with only the data required for the UI.
 * It is created using the school response object which is the API response and has lots of data.
 */
public class SchoolSatScoreModel {
    private String satCriticalReading;
    private String satMath;
    private String satWriting;

    public String getSatCriticalReading() {
        return satCriticalReading;
    }

    public void setSatCriticalReading(String satCriticalReading) {
        this.satCriticalReading = satCriticalReading;
    }

    public String getSatMath() {
        return satMath;
    }

    public void setSatMath(String satMath) {
        this.satMath = satMath;
    }

    public String getSatWriting() {
        return satWriting;
    }

    public void setSatWriting(String satWriting) {
        this.satWriting = satWriting;
    }
}
