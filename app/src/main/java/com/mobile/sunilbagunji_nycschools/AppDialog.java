package com.mobile.sunilbagunji_nycschools;

import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

/**
 * This class implements the dilog used to display error/information message to the user.
 */
public class AppDialog extends DialogFragment {
    public static String TAG = "AppDialog";
    public static String KEY = "message";

    //  TODO:  Make this a dialog to display custom button text, handle positive and negative button etc.
    //  TODO:  Customize it to show error or info message.
    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        String message = getArguments().getString(KEY);

        return new AlertDialog.Builder(requireContext())
                .setMessage(message)
                .setPositiveButton(getString(R.string.ok), (dialog, which) -> {} )
                .create();
    }
}
