package com.mobile.sunilbagunji_nycschools.viewmodel;

import com.mobile.sunilbagunji_nycschools.models.SchoolModel;
import com.mobile.sunilbagunji_nycschools.models.SchoolSatScoreModel;
import com.mobile.sunilbagunji_nycschools.repository.SchoolRepository;
import java.util.List;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

/**
 * This class is the view model class that drives the UI.
 * UI only interacts with this class to get data.
 * It uses a repository to fetch data.
 * It provides the live data (which it gets from repository) instance to the UI.
 * UI uses this live data instance to render UI.
 */
public class SchoolViewModel extends ViewModel {
    private SchoolRepository mSchoolRepository;
    private LiveData<List<SchoolModel>> mSchoolListResponseLiveData;
    private LiveData<SchoolSatScoreModel> mSchoolSatScoreModelLiveData;
    private MutableLiveData<SchoolModel> mSelectedSchoolModel = new MutableLiveData<>();

    public void init(SchoolRepository schoolRepository) {
        //  TODO:  This is done to make it easy to test the repository.
        //  TODO:  Given time, we can use a better approach like Dependency Injection.
        if(schoolRepository == null) {
            mSchoolRepository = new SchoolRepository();
        } else {
            mSchoolRepository = schoolRepository;
        }

        mSchoolListResponseLiveData
                = mSchoolRepository.getSchoolListResponseMutableLiveData();
        mSchoolSatScoreModelLiveData
                = mSchoolRepository.getSchoolSatScoreModelMutableLiveData();
    }

    public void getSchools() {
        mSchoolRepository.getSchools();
    }

    public void getSchoolDetails(String dbn) {
        mSchoolRepository.getSchoolDetails(dbn);
    }

    public LiveData<List<SchoolModel>> getSchoolListResponseLiveData() {
        return mSchoolListResponseLiveData;
    }

    public LiveData<SchoolSatScoreModel> getSchoolSatScoreModelLiveData() {
        return mSchoolSatScoreModelLiveData;
    }

    public void setSelectedSchoolModel(SchoolModel schoolModel) {
        mSelectedSchoolModel.setValue(schoolModel);
    }

    public SchoolModel getSelectedSchoolModel() {
        return mSelectedSchoolModel.getValue();
    }
}
