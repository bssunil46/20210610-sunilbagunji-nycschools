package com.mobile.sunilbagunji_nycschools;

import androidx.appcompat.app.AppCompatActivity;

/**
 * Creating this class to do common tasks like loading a progress dialog etc.
 */
public class BaseActivity extends AppCompatActivity {
}
