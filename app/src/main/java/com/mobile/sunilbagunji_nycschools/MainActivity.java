package com.mobile.sunilbagunji_nycschools;

import android.os.Bundle;

import com.mobile.sunilbagunji_nycschools.ui.SchoolListFragment;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //  Checking if this activity is launched for the first time.
        //  We don't want to add the fragment every-time this activity is created by android system.
        if (savedInstanceState == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .setReorderingAllowed(true)
                    .add(R.id.fragmentContainer, SchoolListFragment.class, null)
                    .commit();
        }
    }
}