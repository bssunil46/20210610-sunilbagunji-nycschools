package com.mobile.sunilbagunji_nycschools.repository;

import com.mobile.sunilbagunji_nycschools.AppConfig;
import com.mobile.sunilbagunji_nycschools.api.SchoolListService;
import com.mobile.sunilbagunji_nycschools.api.SchoolSatScoreService;
import com.mobile.sunilbagunji_nycschools.models.SchoolModel;
import com.mobile.sunilbagunji_nycschools.models.SchoolResponse;
import com.mobile.sunilbagunji_nycschools.models.SchoolSatScoreModel;
import com.mobile.sunilbagunji_nycschools.models.SchoolSatScoreResponse;

import java.util.ArrayList;
import java.util.List;

import androidx.lifecycle.MutableLiveData;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * This class handles data operations.  It provides an API for the rest of the app to retrieve data.
 * It uses the school list and school SAT score service to fetch data from web-service.
 * It uses the LiveData holder to send data directly to UI.
 */

//  TODO:  Given time, we can have a caching mechanism implemented in the repository.
//  TODO:  All the responses can be stored in a cache/db and get data from it when the app is offline.

public class SchoolRepository {
    private SchoolListService mSchoolListService;
    private SchoolSatScoreService mSchoolSatScoreService;
    private MutableLiveData<List<SchoolModel>> mSchoolListResponseMutableLiveData;
    private MutableLiveData<SchoolSatScoreModel> mSchoolSatScoreModelMutableLiveData;

    public SchoolRepository() {
        mSchoolListResponseMutableLiveData = new MutableLiveData<>();
        mSchoolSatScoreModelMutableLiveData = new MutableLiveData<>();

        //  This is to log the request/response.
        //  This should only be done in QA env.  In PROD env, this should be disabled.
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.level(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        Retrofit retrofit = new retrofit2.Retrofit.Builder()
                .baseUrl(AppConfig.BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        mSchoolListService = retrofit.create(SchoolListService.class);
        mSchoolSatScoreService = retrofit.create(SchoolSatScoreService.class);
    }

    public void getSchools() {
        mSchoolListService.getSchools().enqueue(new Callback<List<SchoolResponse>>() {
            @Override
            public void onResponse(Call<List<SchoolResponse>> call, Response<List<SchoolResponse>> response) {
                if(response.isSuccessful() && response.body() != null) {
                    List<SchoolModel> schoolModelList = new ArrayList<>();

                    if(response.body() != null && !response.body().isEmpty()) {
                        for(SchoolResponse schoolResponse : response.body()) {
                            SchoolModel schoolModel = new SchoolModel();
                            schoolModel.setDbn(schoolResponse.getDbn());
                            schoolModel.setName(schoolResponse.getSchoolName());

                            String addressString = schoolResponse.getPrimaryAddressLine1() +
                                    " " +
                                    schoolResponse.getCity() +
                                    " " +
                                    schoolResponse.getZip() +
                                    " " +
                                    schoolResponse.getStateCode();
                            schoolModel.setAddress(addressString);

                            schoolModel.setPhoneNumber(schoolResponse.getPhoneNumber());
                            schoolModel.setEmail(schoolResponse.getSchoolEmail());
                            schoolModel.setDescription(schoolResponse.getOverviewParagraph());

                            schoolModelList.add(schoolModel);
                        }
                    }
                    mSchoolListResponseMutableLiveData.postValue(schoolModelList);
                }
            }

            @Override
            public void onFailure(Call<List<SchoolResponse>> call, Throwable t) {
                //  TODO:  We are handling errors generically by setting data to null.
                //  TODO:  Given time, specify error handling based on HTTP status can be done here.
                mSchoolListResponseMutableLiveData.postValue(null);
            }
        });
    }

    public void getSchoolDetails(String dbn) {
        mSchoolSatScoreService.getSchoolSatScore(dbn).enqueue(new Callback<List<SchoolSatScoreResponse>>() {
            @Override
            public void onResponse(Call<List<SchoolSatScoreResponse>> call, Response<List<SchoolSatScoreResponse>> response) {
                SchoolSatScoreModel schoolSatScoreModel = new SchoolSatScoreModel();
                if(response.isSuccessful() && response.body() != null && !response.body().isEmpty()) {
                    schoolSatScoreModel.setSatCriticalReading(response.body().get(0).getSatCriticalReadingAvgScore());
                    schoolSatScoreModel.setSatMath(response.body().get(0).getSatMathAvgScore());
                    schoolSatScoreModel.setSatWriting(response.body().get(0).getSatWritingAvgScore());
                }
                mSchoolSatScoreModelMutableLiveData.postValue(schoolSatScoreModel);
            }

            @Override
            public void onFailure(Call<List<SchoolSatScoreResponse>> call, Throwable t) {
                mSchoolSatScoreModelMutableLiveData.postValue(null);
            }
        });
    }

    public MutableLiveData<SchoolSatScoreModel> getSchoolSatScoreModelMutableLiveData() {
        return mSchoolSatScoreModelMutableLiveData;
    }

    public MutableLiveData<List<SchoolModel>> getSchoolListResponseMutableLiveData() {
        return mSchoolListResponseMutableLiveData;
    }
}
