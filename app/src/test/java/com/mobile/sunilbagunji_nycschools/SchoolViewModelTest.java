package com.mobile.sunilbagunji_nycschools;

import com.mobile.sunilbagunji_nycschools.repository.SchoolRepository;
import com.mobile.sunilbagunji_nycschools.viewmodel.SchoolViewModel;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class SchoolViewModelTest {
    @Mock
    SchoolRepository mockSchoolRepository;
    SchoolViewModel schoolViewModel;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        schoolViewModel = new SchoolViewModel();
        schoolViewModel.init(mockSchoolRepository);
    }

    @Test
    public void getSchoolsTest() {
        schoolViewModel.getSchools();
        Mockito.verify(mockSchoolRepository).getSchools();
    }

    @Test
    public void getSchoolDetailsTest() {
        schoolViewModel.getSchoolDetails("12345");
        Mockito.verify(mockSchoolRepository).getSchoolDetails("12345");
    }
}