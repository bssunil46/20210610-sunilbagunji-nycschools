# Requirements

- Display a list of NYC schools. 
- Selecting a school should display SAT scores of the school (for reading, writing and math).
- Additional information has to be displayed on details screen.

### Additional Requirements 
 -  Filter school by name.
 -  In school list and details screen,  on tapping address, phone number and email, launch corresponding app (map, phone and email app).

 ### Bonus
 -  Unit test for view model. 

 ### API Endpoints
  -  Get list of schools https://data.cityofnewyork.us/Education/DOE-High-School-Directory-2017/s3k6-pzi2
  -  Get SAT score of a school https://data.cityofnewyork.us/Education/SAT-Results/f9bf-2cp4

# App Architecture

The app is implemented based on the reference architecure recommended by Google for android apps.  

It is based on the following two architectural principals

 - Separation of Conerns:  This is done by using the MVVM architectural pattern.  MVVM uses data binding technique, which decouples view and view model.  

 - Driving the UI using a view model:  The is done using view model and live data architectural components.

Find below the high level architecture of the app

<img src="NYC_Arch.png">

# ScreenShots

<img src="NYC_School_List_Screenshot.png" width="270" height="540"> <img src="NYC_School_Details_Screenshot.png" width="270" height="540"> 




